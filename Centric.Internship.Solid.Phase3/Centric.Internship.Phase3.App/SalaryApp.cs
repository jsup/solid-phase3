﻿using Centric.Internship.Phase3.Business;
using Centric.Internship.Phase3.Implementations;

namespace Centric.Internship.Phase3.App
{
    public class SalaryApp
    {
        public void Run()
        {
            var fileReaderService = new FileReaderService();
            fileReaderService.RegisterFileReader(new XMLFileReader());
            fileReaderService.RegisterFileReader(new JSONFileReader());

            var salaryCalculator = new SalaryCalculator(fileReaderService);
            salaryCalculator.CalculateAverageSalary();
        }
    }
}
