﻿using Centric.Internship.Phase3.Interfaces;
using Centric.Internship.Phase3.Models;
using System.Collections.Generic;

namespace Centric.Internship.Phase3.Business
{
    public class FileReaderService
    {
        private IList<IFileReader> _fileReaders;

        public FileReaderService()
        {
            _fileReaders = new List<IFileReader>();
        }

        public void RegisterFileReader(IFileReader reader)
        {
            _fileReaders.Add(reader);
        }

        public Employees Read(string filePath)
        {
            Employees employees = null;

            foreach (var fileReader in _fileReaders)
            {
                if (fileReader.CanRead(filePath))
                {
                    employees = fileReader.Read(filePath);
                    break;
                }
            }

            return employees;
        }
    }
}
