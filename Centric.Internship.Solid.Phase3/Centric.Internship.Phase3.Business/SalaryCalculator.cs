﻿using Centric.Internship.Phase3.Models;
using Newtonsoft.Json;
using System.IO;
using System.Xml.Serialization;

namespace Centric.Internship.Phase3.Business
{
    public class SalaryCalculator
    {
        private FileReaderService _fileReaderService;
        private AverageSalaryCalculator _averageSalaryCalculator;

        public SalaryCalculator(FileReaderService fileReaderService)
        {
            _fileReaderService = fileReaderService;
            _averageSalaryCalculator = new AverageSalaryCalculator();
        }

        public void CalculateAverageSalary()
        {
            var employees = _fileReaderService.Read(@"..\..\..\..\SalariesPerYear.xml");
            if (employees == null)
            {
                employees = _fileReaderService.Read(@"..\..\..\..\SalariesPerYear.json");
            }

            var results = _averageSalaryCalculator.CalculateAverageSalary(employees);

            this.OutputResults(results);
        }

        private void OutputResults(Results results)
        {
            var xmlSerializer = new XmlSerializer(typeof(Results));
            using (var fileStream = new FileStream(@"..\..\..\..\ResultsPerYear.xml", FileMode.Create))
            {
                xmlSerializer.Serialize(fileStream, results);
            }
        }
    }
}
