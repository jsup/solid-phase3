﻿using System;
using System.Xml.Serialization;

namespace Centric.Internship.Phase3.Models
{

    [Serializable()]
    [XmlType("salary")]
    public class Salary
    {
        [XmlElement(ElementName = "year")]
        public string Year { get; set; }

        [XmlElement(ElementName = "amount")]
        public double Amount { get; set; }
    }
}