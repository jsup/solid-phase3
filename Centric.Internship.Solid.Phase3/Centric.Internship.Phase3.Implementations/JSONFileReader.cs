﻿using Centric.Internship.Phase3.Interfaces;
using Centric.Internship.Phase3.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;

namespace Centric.Internship.Phase3.Implementations
{
    public class JSONFileReader : IFileReader
    {
        public bool CanRead(string filePath)
        {
            try
            {
                using (StreamReader r = new StreamReader(filePath))
                {

                    var json = r.ReadToEnd();
                    var jobj = JObject.Parse(json);
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public Employees Read(string filePath)
        {
            Employees employees = null;

            using (StreamReader r = new StreamReader(filePath))
            {
                string json = r.ReadToEnd();
                employees = JsonConvert.DeserializeObject<Employees>(json);
            }

            return employees;
        }
    }
}
